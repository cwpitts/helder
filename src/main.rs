use clap;
use clap::{Parser, Subcommand};
use dirs;
use huelib;
use huelib::resource::Modifier;
use huelib::resource::{group, light, Adjust, Alert, Effect, Group, Light};
use log::{debug, error, LevelFilter};
use log4rs::append::console::ConsoleAppender;
use log4rs::config::{Appender, Logger, Root};
use log4rs::Config;
use serde_json;
use std::error::Error;
use std::fs::File;
use std::io::Write;
use std::net::{IpAddr, Ipv4Addr};
use std::path::PathBuf;
use termcolor::{BufferWriter, Color, ColorChoice, ColorSpec, WriteColor};

#[derive(Parser)]
#[clap(author="cwpitts", version="0.1.0", about="Philips Hue CLI", long_about=None)]
struct Args {
    #[clap(subcommand)]
    sub_cmd: SubCmd,
}

#[derive(Subcommand)]
enum SubCmd {
    /// Set
    Set(SetArgs),
    /// Show
    Show(ShowArgs),
}

#[derive(Subcommand)]
enum ItemType {
    /// Light resource
    Light(ResourceArgs),
    /// Group resource
    Group(ResourceArgs),
}

#[derive(Parser)]
struct ResourceArgs {
    #[clap(help = "Resource or name")]
    id_or_name: Vec<String>,
}

#[derive(Subcommand)]
enum ShowItemType {
    /// Light resource
    Light(ShowItemArgs),
    /// Group resource
    Group(ShowItemArgs),
}

#[derive(Parser)]
struct ShowItemArgs {
    #[clap(help = "Integer ID of light")]
    id: Option<usize>,
    #[clap(short = 'a', long = "all", help = "Show all resources")]
    all: bool,
}

#[derive(Parser)]
struct ShowArgs {
    #[clap(subcommand)]
    class: ShowItemType,
}

#[derive(Parser)]
struct SetArgs {
    #[clap(subcommand)]
    class: ItemType,
    #[clap(long = "on", help = "Turn light on")]
    on: bool,
    #[clap(long = "off", help = "Turn light off")]
    off: bool,
    #[clap(short = 'b', long = "brightness", help = "Brightness level")]
    brightness: Option<u8>,
    #[clap(short = 'c', long = "colortemp", help = "Color temperature")]
    ct: Option<u16>,
    #[clap(long = "hue", help = "Hue")]
    hue: Option<u16>,
    #[clap(short = 's', long = "saturation", help = "Saturation")]
    sat: Option<u8>,
    #[clap(long = "xy", help = "XY")]
    xy: Option<String>,
    #[clap(short = 'a', long = "alert", help = "Set alert status", arg_enum)]
    alert: Option<AlertType>,
    #[clap(short = 'e', long = "effect", help = "Set effect status", arg_enum)]
    effect: Option<EffectType>,
    #[clap(short = 't', long = "transition-time", help = "Set transition time")]
    transition_time: Option<u16>,
    #[clap(long = "scene", help = "Set scene")]
    scene: Option<String>,
}

#[derive(clap::ArgEnum, Clone, Debug)]
enum AlertType {
    Select,
    LSelect,
    None,
}

impl AlertType {
    fn to_alert(self: &Self) -> Alert {
        match self {
            AlertType::Select => {
                return Alert::Select;
            }
            AlertType::LSelect => {
                return Alert::LSelect;
            }
            AlertType::None => {
                return Alert::None;
            }
        }
    }
}

#[derive(clap::ArgEnum, Clone, Debug)]
enum EffectType {
    ColorLoop,
    None,
}

impl EffectType {
    fn to_effect(self: &Self) -> Effect {
        match self {
            EffectType::ColorLoop => {
                return Effect::Colorloop;
            }
            EffectType::None => {
                return Effect::None;
            }
        }
    }
}

struct HelderConfig {
    ip_addr: IpAddr,
    username: String,
}

impl HelderConfig {
    fn from_file(config_path: PathBuf) -> HelderConfig {
        let config_file: File =
            File::open(config_path.as_path()).expect("Could not open config file!");
        let json: serde_json::Value =
            serde_json::from_reader(config_file).expect("Could not read config file!");

        let ip_addr: IpAddr = json["bridge_ip"]
            .as_str()
            .unwrap()
            .parse::<IpAddr>()
            .expect("Could not parse IP address!");

        let config: HelderConfig = HelderConfig {
            ip_addr: ip_addr,
            username: json["bridge_username"].as_str().unwrap().to_string(),
        };
        return config;
    }
}

/// Parse x, y coordinates in color space
///
/// # Arguments
/// * `s` - String value that should be in x,y form
fn parse_xy(s: String) -> Result<(f32, f32), String> {
    let split: Vec<&str> = s.split(",").collect();

    if split.len() != 2 {
        return Err("Could not parse xy".to_string());
    }

    let x_parsed = split[0].parse::<f32>();
    if let Err(_) = x_parsed {
        return Err("Could not parse x value".to_string());
    }

    let y_parsed = split[0].parse::<f32>();
    if let Err(_) = y_parsed {
        return Err("Could not parse y value".to_string());
    }

    return Ok((x_parsed.unwrap(), y_parsed.unwrap()));
}

/// Load config file
///
/// Load a config file from disk, or initializes new config file
/// and walks through setup.
fn load_config() -> HelderConfig {
    let mut config_path: PathBuf = dirs::home_dir().unwrap();
    config_path.push(".helder");
    config_path.push("config.json");
    if config_path.exists() {
        let config: HelderConfig = HelderConfig::from_file(config_path);
        return config;
    } else {
        let config: HelderConfig = HelderConfig {
            ip_addr: IpAddr::V4(Ipv4Addr::new(192, 168, 1, 2)),
            username: "nH9pUDjwHYUl3cpMk5uxH9Z060wTgvtmIBu-qEvd".to_string(),
        };
        return config;
    }
}

/// Print light
///
/// Prints attributes of light retrieved from bridge, with some nice
/// color formatting
///
/// # Arguments
/// * `light` - Discovered light from bridge
fn print_light(light: &Light) -> Result<(), std::io::Error> {
    let bufwtr = BufferWriter::stdout(ColorChoice::Always);
    let mut buffer = bufwtr.buffer();

    buffer.set_color(ColorSpec::new().set_underline(true))?;
    writeln!(&mut buffer, "Light {}", light.id)?;
    buffer.reset()?;

    writeln!(&mut buffer, "Name: \"{}\"", light.name)?;

    write!(&mut buffer, "State: ")?;
    match light.state.on {
        Some(light_is_on) => {
            if light_is_on {
                buffer.set_color(ColorSpec::new().set_fg(Some(Color::Yellow)).set_bold(true))?;
                writeln!(&mut buffer, "On")?;
                buffer.reset()?;
            } else {
                buffer.set_color(ColorSpec::new().set_fg(Some(Color::Blue)).set_dimmed(true))?;
                writeln!(&mut buffer, "Off")?;
                buffer.reset()?;
            }
        }
        None => writeln!(&mut buffer, "Light state: Not available")?,
    }
    write!(&mut buffer, "Brightness: ")?;
    let brightness = light.state.brightness.unwrap();
    if brightness < 84 {
        buffer.set_color(ColorSpec::new().set_dimmed(true))?;
    } else if brightness > 170 {
        buffer.set_color(ColorSpec::new().set_bold(true))?;
    }
    writeln!(&mut buffer, "{}", brightness)?;
    buffer.reset()?;

    match light.state.hue {
        Some(hue) => writeln!(&mut buffer, "Hue: {}", hue)?,
        None => writeln!(&mut buffer, "Hue: N/A")?,
    }

    match light.state.saturation {
        Some(saturation) => writeln!(&mut buffer, "Saturation: {}", saturation)?,
        None => writeln!(&mut buffer, "Saturation: N/A")?,
    }

    match light.state.color_temperature {
        Some(color_temperature) => writeln!(&mut buffer, "Color tone: {}", color_temperature)?,
        None => writeln!(&mut buffer, "Color tone: N/A")?,
    }

    match light.state.color_space_coordinates {
        Some(xy) => writeln!(&mut buffer, "XY: ({}, {})", xy.0, xy.1)?,
        None => writeln!(&mut buffer, "XY: N/A")?,
    }

    writeln!(&mut buffer, "")?;
    bufwtr.print(&buffer)?;

    return Ok(());
}

/// Print group
///
/// Prints group of lights discovered from bridge
///
/// # Arguments
/// * `bridge` - Hue Bridge
/// * `group` - Group discovered from the bridge
fn print_group(bridge: &huelib::Bridge, group: &Group) -> Result<(), std::io::Error> {
    let bufwtr = BufferWriter::stdout(ColorChoice::Always);
    let mut buffer = bufwtr.buffer();

    buffer.set_color(ColorSpec::new().set_underline(true))?;
    writeln!(&mut buffer, "Group {}", group.id)?;
    buffer.reset()?;

    bufwtr.print(&buffer)?;

    for light_id in &group.lights {
        let light: Light = bridge.get_light(light_id).unwrap();
        print_light(&light)?;
    }

    return Ok(());
}

/// Handle show action
///
/// Handles action for displaying lights or groups
///
/// # Arguments
/// * `bridge` - Hue bridge
/// * `args` - Arguments for display command
fn handle_show_action(bridge: huelib::Bridge, args: &ShowArgs) -> Result<(), Box<dyn Error>> {
    match &args.class {
        ShowItemType::Light(item_args) => match item_args.all {
            true => match bridge.get_all_lights() {
                Ok(lights) => {
                    for light in lights {
                        print_light(&light)?;
                    }
                }
                Err(err) => {
                    return Err(format!("Failed to iterate lights: {}", err).into());
                }
            },
            false => match bridge.get_light(item_args.id.unwrap().to_string()) {
                Ok(light) => {
                    print_light(&light)?;
                }
                Err(err) => {
                    return Err(format!("Failed to iterate lights: {}", err).into());
                }
            },
        },
        ShowItemType::Group(item_args) => match item_args.all {
            true => match bridge.get_all_groups() {
                Ok(groups) => {
                    for group in groups {
                        print_group(&bridge, &group)?;
                    }
                }
                Err(err) => {
                    return Err(format!("Failed to iterate lights: {}", err).into());
                }
            },
            false => match bridge.get_group(item_args.id.unwrap().to_string()) {
                Ok(group) => {
                    print_group(&bridge, &group)?;
                }
                Err(err) => {
                    return Err(format!("Failed to iterate lights: {}", err).into());
                }
            },
        },
    }

    return Ok(());
}

/// Build command
///
/// Constructs command to apply to objects
///
/// # Arguments
/// * `args` - Arguments describing the state mutation
fn build_group_command(args: &SetArgs) -> group::StateModifier {
    let mut modifier = group::StateModifier::new();

    match args.brightness {
        Some(brightness) => {
            modifier = modifier.with_brightness(Adjust::Override(brightness));
        }
        None => {}
    }

    if args.on {
        modifier = modifier.with_on(true);
    } else if args.off {
        modifier = modifier.with_on(false);
    }

    match args.hue {
        Some(hue) => {
            modifier = modifier.with_hue(Adjust::Override(hue));
        }
        None => {}
    }

    match args.ct {
        Some(ct) => {
            modifier = modifier.with_color_temperature(Adjust::Override(ct));
        }
        None => {}
    }

    match args.xy {
        Some(ref xy_str) => match parse_xy(xy_str.to_string()) {
            Ok(xy) => {
                modifier = modifier.with_color_space_coordinates(Adjust::Override(xy));
            }
            Err(_) => {
                error!("Failed to parse color coordinates");
            }
        },
        None => {}
    }

    match args.alert {
        Some(ref alert) => {
            modifier = modifier.with_alert(alert.to_alert());
        }
        None => {}
    }

    match args.effect {
        Some(ref effect) => {
            modifier = modifier.with_effect(effect.to_effect());
        }
        None => {}
    }

    match args.transition_time {
        Some(time) => {
            modifier = modifier.with_transition_time(time);
        }
        None => {}
    }

    match args.scene {
        Some(ref scene) => {
            modifier = modifier.with_scene(scene.to_string());
        }
        None => {}
    }

    return modifier;
}

/// Build command
///
/// Constructs command to apply to objects
///
/// # Arguments
/// * `args` - Arguments describing the state mutation
fn build_light_command(args: &SetArgs) -> light::StateModifier {
    let mut modifier = light::StateModifier::new();

    match args.brightness {
        Some(brightness) => {
            modifier = modifier.with_brightness(Adjust::Override(brightness));
        }
        None => {}
    }

    if args.on {
        modifier = modifier.with_on(true);
    } else if args.off {
        modifier = modifier.with_on(false);
    }

    match args.hue {
        Some(hue) => {
            modifier = modifier.with_hue(Adjust::Override(hue));
        }
        None => {}
    }

    match args.ct {
        Some(ct) => {
            modifier = modifier.with_color_temperature(Adjust::Override(ct));
        }
        None => {}
    }

    match args.xy {
        Some(ref xy_str) => match parse_xy(xy_str.to_string()) {
            Ok(xy) => {
                modifier = modifier.with_color_space_coordinates(Adjust::Override(xy));
            }
            Err(_) => {
                error!("Failed to parse color coordinates");
            }
        },
        None => {}
    }

    match args.alert {
        Some(ref alert) => {
            modifier = modifier.with_alert(alert.to_alert());
        }
        None => {}
    }

    match args.effect {
        Some(ref effect) => {
            modifier = modifier.with_effect(effect.to_effect());
        }
        None => {}
    }

    match args.transition_time {
        Some(time) => {
            modifier = modifier.with_transition_time(time);
        }
        None => {}
    }

    return modifier;
}

/// Interact with group objects
///
/// Controls group objects
///
/// # Arguments
/// * `bridge` - Hue bridge
/// * `args` - Arguments for mutating the state of the group(s)
fn interact_with_groups(bridge: &huelib::Bridge, args: &SetArgs) -> Result<(), Box<dyn Error>> {
    let cmd: group::StateModifier = build_group_command(args);

    match &args.class {
        ItemType::Group(ref resource_args) => {
            for id in &resource_args.id_or_name {
                debug!("Executing command for group {}", id);
                match cmd.execute(bridge, id.to_string()) {
                    Ok(_) => {}
                    Err(err) => {
                        error!("Failed to control group {}: {}", id, err);
                    }
                }
            }
        }
        _ => {}
    }

    Ok(())
}

/// Interact with light objects
///
/// Controls light objects
///
/// # Arguments
/// * `bridge` - Hue bridge
/// * `args` - Arguments for mutating the state of the light(s)
fn interact_with_lights(bridge: &huelib::Bridge, args: &SetArgs) -> Result<(), Box<dyn Error>> {
    let cmd: light::StateModifier = build_light_command(args);

    match &args.class {
        ItemType::Light(ref resource_args) => {
            for id in &resource_args.id_or_name {
                debug!("Executing command for light {}", id);
                match cmd.execute(bridge, id.to_string()) {
                    Ok(_) => {}
                    Err(err) => {
                        error!("Failed to control light {}: {}", id, err);
                    }
                }
            }
        }
        _ => {}
    }

    Ok(())
}

fn main() {
    let args: Args = Args::parse();

    let stdout = ConsoleAppender::builder().build();
    let config = Config::builder()
        .appender(Appender::builder().build("stdout", Box::new(stdout)))
        .logger(Logger::builder().build("helder", LevelFilter::Debug))
        .build(Root::builder().appender("stdout").build(LevelFilter::Error))
        .unwrap();
    let _handle = log4rs::init_config(config).unwrap();

    debug!("Logging configured");

    let config: HelderConfig = load_config();
    let bridge: huelib::Bridge = huelib::Bridge::new(config.ip_addr, config.username);

    debug!("Configured Bridge client");

    match args.sub_cmd {
        SubCmd::Set(ref sub_cmd) => match sub_cmd.class {
            ItemType::Light(_) => match interact_with_lights(&bridge, &sub_cmd) {
                Ok(_) => {}
                Err(err) => {
                    error!("Error interacting with lights:\n{}", err);
                }
            },
            ItemType::Group(_) => match interact_with_groups(&bridge, &sub_cmd) {
                Ok(_) => {}
                Err(err) => {
                    error!("Error interacting with groups:\n{}", err);
                }
            },
        },
        SubCmd::Show(ref sub_cmd) => match handle_show_action(bridge, sub_cmd) {
            Ok(_) => {}
            Err(err) => {
                error!("Error displaying lights:\n{}", err);
            }
        },
    }
}
