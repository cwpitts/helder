.PHONY           := clean
.DEFAULT_TARGET  := all

all:
	cargo build

test:
	cargo test

clean:
	rm -rf target/

format:
	find . -name '*.rs' -exec rustfmt {} \;
