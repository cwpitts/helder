# helder
## Description
[`helder`](https://translate.google.com/#nl/en/helder) is a command-line interface for controlling individual and grouped Philips Hue bulbs. Designed for one-off command line usage and usage in shell scripts, helder provides a flexible, easy-to-understand method for controlling Hue light fixtures.

## Installation
`helder` is written in Go, so a "go build" should suffice.

### Verifying installation
Test your installation with `helder -v`

## Usage
The output of `helder -h`:
```
Usage of helder:
helder [-all | -help | -version | <cmd>]
cmd	:= [light | group] <action>
action	:= [on | off | set <attr>]
attr	:= any available attribute on the Hue lights

Flags:
  -all
    	Apply command to all resources
  -help
    	Show help and exit
  -version
    	Show version and exit
```

When you run `helder` for the first time, it will walk you through initializing your connection with the Hue Bridge, including creating authentication tokens to use with the Bridge.

### Examples
```
# Turning all the lights in group 1 off:
helder group 1 set off

# Turning the light named "bulb 1" on with brightness level 75 (0-255):
helder light "bulb 1" on
helder light "bulb 1" set bri=75
```
